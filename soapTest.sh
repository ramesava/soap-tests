#!/bin/bash
echo "Starting Tests"

/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/Order-Tracking-soapui-project.xml -r -f/test.xml
/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/TCOOL-Order-detail-soapui-project.xml -r -f/test.xml
/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/TCOOL-Order-History-soapui-project.xml -r -f/test/xml
/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/PCOOL-OrderHistory-PROD-soapui-project.xml -r -f/test/xml
/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/PCOOL-Order-History--NEW-soapui-project.xml -r -f/test/xml
/opt/SmartBear/SoapUI-5.2.1/bin/testrunner.sh /opt/SmartBear/SoapUI-5.2.1/TRACKING-TCOOL-soapui-project.xml -r -f/test/xml

echo "Tests Done"
